<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H6';

$current_user_parent_role_seq='H1::H6';

$current_user_profiles=array(5,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>0,'2'=>1,'4'=>0,'6'=>0,'7'=>1,'8'=>0,'9'=>1,'10'=>0,'13'=>1,'14'=>0,'15'=>0,'16'=>1,'18'=>1,'19'=>0,'20'=>0,'21'=>0,'22'=>0,'23'=>0,'24'=>1,'25'=>0,'26'=>1,'27'=>1,'30'=>1,'31'=>0,'32'=>0,'33'=>0,'34'=>0,'35'=>1,'36'=>1,'37'=>1,'38'=>1,'39'=>1,'40'=>0,'41'=>0,'42'=>0,'43'=>1,'45'=>0,'46'=>1,'47'=>1,'48'=>1,'49'=>0,'50'=>1,'51'=>0,'52'=>0,'28'=>0,'3'=>0,);

$profileActionPermission=array(2=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>1,6=>1,10=>0,),4=>array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>1,6=>1,8=>0,10=>0,),6=>array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>1,6=>1,8=>0,10=>0,),7=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>1,6=>1,8=>0,9=>0,10=>0,),8=>array(0=>0,1=>0,2=>0,3=>0,4=>0,6=>1,),9=>array(0=>1,1=>1,2=>1,3=>1,4=>1,),13=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>1,6=>1,8=>0,10=>0,),14=>array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>1,6=>1,10=>0,),15=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),16=>array(0=>1,1=>1,2=>1,3=>1,4=>1,),18=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>0,6=>0,10=>0,),19=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),20=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),21=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),22=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),23=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),26=>array(0=>1,1=>1,2=>1,3=>1,4=>1,),36=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>1,6=>1,8=>1,),37=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>0,6=>0,10=>0,),38=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>0,6=>0,10=>0,),41=>array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,10=>0,),45=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),46=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>0,6=>0,10=>0,),47=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>0,6=>0,10=>0,),48=>array(0=>1,1=>1,2=>1,3=>1,4=>1,5=>0,6=>0,10=>0,),50=>array(0=>1,1=>1,2=>1,3=>1,4=>1,),);

$current_user_groups=array();

$subordinate_roles=array();

$parent_roles=array('H1',);

$subordinate_roles_users=array();

$user_info=array('user_name'=>'acountmanager','is_admin'=>'off','user_password'=>'$1$ac000000$1VfkeyYU8jmMzHfqcIdwq.','confirm_password'=>'$1$ac000000$1VfkeyYU8jmMzHfqcIdwq.','first_name'=>'Account','last_name'=>'Manager','roleid'=>'H6','email1'=>'sanjitb.bauli@jhunsinfotech.com','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'','end_hour'=>'','start_hour'=>'','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'YjartPXm0lN7IKZi','time_zone'=>'UTC','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'','currency_grouping_separator'=>'','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'1','theme'=>'softed','language'=>'en_us','reminder_interval'=>'None','asterisk_extension'=>'','use_asterisk'=>'1','ccurrency_name'=>'','currency_code'=>'GBP','currency_symbol'=>'&#163;','conv_rate'=>'1.000','record_id'=>'','record_module'=>'','currency_name'=>'United Kingdom, Pounds','id'=>'5');
?>